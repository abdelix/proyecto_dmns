----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.12.2015 19:50:18
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( clk : in STD_LOGIC;
           switch : in STD_LOGIC;
           red : out STD_LOGIC_VECTOR (4 downto 0);
           green : out STD_LOGIC_VECTOR (5 downto 0);
           blue : out STD_LOGIC_VECTOR (4 downto 0);
           hsync : out STD_LOGIC;
           vsync : out STD_LOGIC);
end top;

architecture Behavioral of top is

signal column_int, row_int : integer;
signal column_logic, row_logic : STD_LOGIC_VECTOR(31 downto 0);
signal disp_enable, disp_rst : STD_LOGIC;
signal CLK_25,locked : STD_LOGIC; 

-- componente PLL
component clk_wiz_0
port
 (-- Clock in ports
  clk_in1           : in     std_logic;
  -- Clock out ports
  clk_out1          : out    std_logic;
  -- Status and control signals
  reset             : in     std_logic;
  locked            : out    std_logic
 );
end component;

ATTRIBUTE SYN_BLACK_BOX : BOOLEAN;
ATTRIBUTE SYN_BLACK_BOX OF clk_wiz_0 : COMPONENT IS TRUE;


ATTRIBUTE BLACK_BOX_PAD_PIN : STRING;
ATTRIBUTE BLACK_BOX_PAD_PIN OF clk_wiz_0 : COMPONENT IS "clk_in1,clk_out1,reset,locked";
begin
--- PLL
myPLL_inst : clk_wiz_0 port map (clk_in1=> CLK , clk_out1=> CLK_25, reset => switch,locked=>locked );

vga_controller_inst : entity vga_controller
 port map (
   pixel_clk => CLK_25,
   reset_n => disp_rst,
   disp_ena => disp_enable,
   h_sync => hsync,
   v_sync => vsync,
   row => row_int,
   column => column_int
);

color_board_gen_inst : entity color_board_gen

port map (
   disp_ena => disp_enable,
   row => row_logic,
   column => column_logic,
   r => red,
   g => green,
   b => blue
);

disp_rst<=not locked;
column_int <= TO_INTEGER(unsigned(column_logic));
row_int <= TO_INTEGER(unsigned(row_logic));

end Behavioral;
