----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.12.2015 19:38:33
-- Design Name: 
-- Module Name: color_board_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity color_board_gen is
    Port ( disp_ena : in STD_LOGIC;
           row : in STD_LOGIC_VECTOR (31 downto 0);
           column : in STD_LOGIC_VECTOR (31 downto 0);
           r : out STD_LOGIC_VECTOR (4 downto 0);
           g : out STD_LOGIC_VECTOR (5 downto 0);
           b : out STD_LOGIC_VECTOR (4 downto 0));
end color_board_gen;

architecture Behavioral of color_board_gen is

begin

r <= row(7 downto 3);
g <= column(8 downto 3);
b <= "00000"; --row(2 downto 0)&column(2 downto 1);

end Behavioral;
