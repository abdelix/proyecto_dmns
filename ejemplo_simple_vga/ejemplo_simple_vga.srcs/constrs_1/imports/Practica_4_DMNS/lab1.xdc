## ZYBO Pin Assignments
#############################
## On-board Slide Switches  #
#############################
#set_property PACKAGE_PIN G15 [get_ports sw_4bit_tri_i[0]]
#set_property IOSTANDARD LVCMOS33 [get_ports sw_4bit_tri_i[0]]
#set_property PACKAGE_PIN P15 [get_ports sw_4bit_tri_i[1]]
#set_property IOSTANDARD LVCMOS33 [get_ports sw_4bit_tri_i[1]]
#set_property PACKAGE_PIN W13 [get_ports sw_4bit_tri_i[2]]
#set_property IOSTANDARD LVCMOS33 [get_ports sw_4bit_tri_i[2]]
#set_property PACKAGE_PIN T16 [get_ports sw_4bit_tri_i[3]]
#set_property IOSTANDARD LVCMOS33 [get_ports sw_4bit_tri_i[3]]

set_property PACKAGE_PIN G15 [get_ports switch]
set_property IOSTANDARD LVCMOS33 [get_ports switch]

#############################
## On-board leds            #
#############################
#set_property PACKAGE_PIN M14 [get_ports led_4bit_tri_o[0]]
#set_property IOSTANDARD LVCMOS33 [get_ports led_4bit_tri_o[0]]
#set_property PACKAGE_PIN M15 [get_ports led_4bit_tri_o[1]]
#set_property IOSTANDARD LVCMOS33 [get_ports led_4bit_tri_o[1]]
#set_property PACKAGE_PIN G14 [get_ports led_4bit_tri_o[2]]
#set_property IOSTANDARD LVCMOS33 [get_ports led_4bit_tri_o[2]]
#set_property PACKAGE_PIN D18 [get_ports led_4bit_tri_o[3]]
#set_property IOSTANDARD LVCMOS33 [get_ports led_4bit_tri_o[3]]

#############################
## On-board BTN0 Push-button#
#############################
#set_property PACKAGE_PIN R18 [get_ports btn0_tri_io[0]]
#set_property IOSTANDARD LVCMOS33 [get_ports btn0_tri_io[0]]

#############################
######### Pines VGA #########
#############################

#RED#
set_property PACKAGE_PIN F19 [get_ports red[4]]
set_property IOSTANDARD LVCMOS33 [get_ports red[4]]
set_property PACKAGE_PIN G20 [get_ports red[3]]
set_property IOSTANDARD LVCMOS33 [get_ports red[3]]
set_property PACKAGE_PIN J20 [get_ports red[2]]
set_property IOSTANDARD LVCMOS33 [get_ports red[2]]
set_property PACKAGE_PIN L20 [get_ports red[1]]
set_property IOSTANDARD LVCMOS33 [get_ports red[1]]
set_property PACKAGE_PIN M19 [get_ports red[0]]
set_property IOSTANDARD LVCMOS33 [get_ports red[0]]

#GREEN#
set_property PACKAGE_PIN F20 [get_ports green[5]]
set_property IOSTANDARD LVCMOS33 [get_ports green[5]]
set_property PACKAGE_PIN H20 [get_ports green[4]]
set_property IOSTANDARD LVCMOS33 [get_ports green[4]]
set_property PACKAGE_PIN J19 [get_ports green[3]]
set_property IOSTANDARD LVCMOS33 [get_ports green[3]]
set_property PACKAGE_PIN L19 [get_ports green[2]]
set_property IOSTANDARD LVCMOS33 [get_ports green[2]]
set_property PACKAGE_PIN N20 [get_ports green[1]]
set_property IOSTANDARD LVCMOS33 [get_ports green[1]]
set_property PACKAGE_PIN H18 [get_ports green[0]]
set_property IOSTANDARD LVCMOS33 [get_ports green[0]]

#BLUE#
set_property PACKAGE_PIN G19 [get_ports blue[4]]
set_property IOSTANDARD LVCMOS33 [get_ports blue[4]]
set_property PACKAGE_PIN J18 [get_ports blue[3]]
set_property IOSTANDARD LVCMOS33 [get_ports blue[3]]
set_property PACKAGE_PIN K19 [get_ports blue[2]]
set_property IOSTANDARD LVCMOS33 [get_ports blue[2]]
set_property PACKAGE_PIN M20 [get_ports blue[1]]
set_property IOSTANDARD LVCMOS33 [get_ports blue[1]]
set_property PACKAGE_PIN P20 [get_ports blue[0]]
set_property IOSTANDARD LVCMOS33 [get_ports blue[0]]

#HS#
set_property PACKAGE_PIN P19 [get_ports hsync]
set_property IOSTANDARD LVCMOS33 [get_ports hsync]

#VS#
set_property PACKAGE_PIN R19 [get_ports vsync]
set_property IOSTANDARD LVCMOS33 [get_ports vsync]

#Clock#
set_property PACKAGE_PIN L16 [get_ports clk]
set_property IOSTANDARD LVCMOS33 [get_ports clk]
